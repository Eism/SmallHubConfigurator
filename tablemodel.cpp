/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the examples of the QtSerialBus module.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "tablemodel.h"

#include <QCheckBox>
#include <QTableWidgetItem>
#include <QDebug>

TableModel::TableModel(int _nRows,int _nColumns,int _input, QObject *parent)
    : QAbstractTableModel(parent),
      nRows(_nRows), nColumns(_nColumns), input(_input)
{
}

TableModel::~TableModel(){}


int TableModel::rowCount(const QModelIndex &parent) const
{
    return nRows;
}
int TableModel::columnCount(const QModelIndex &parent) const
{
    return nColumns;
}



QVariant TableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid()){
        return QVariant();
    }

    switch (role) {
    case Qt::CheckStateRole:{
        bool stateIndex= mDevice->GetState(input, index.row(), static_cast<MDevice::Phases>(index.column()));

        auto b = static_cast<Qt::CheckState>(stateIndex);
        return stateIndex ? Qt::Checked : Qt::Unchecked;
        return b;
    }
    case Qt::DisplayRole:{
        return QVariant();
    }
    }

    return QVariant();
}

bool TableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid())
        return false;

    switch (role) {
    case Qt::EditRole:
        //mHash[index] = value;
        emit dataChanged(index,index);
        return false;
        break;
    case Qt::CheckStateRole:
        auto s = static_cast<Qt::CheckState>(value.toUInt());
        mHash[index] = s;
        mDevice->SetState(input,index.row(),static_cast<MDevice::Phases>(index.column()),(s==Qt::Checked)?1:0);
        //bool stateIndex= mDevice->GetState(input, index.row(), static_cast<MDevice::Phases>(index.column()));

        emit dataChanged(index, index);
        return true;
        break;
    }

    return false;
}

Qt::ItemFlags TableModel::flags(const QModelIndex &index) const
{
    Qt::ItemFlags flags = QAbstractTableModel::flags(index);
    return index.isValid() ? (flags| Qt::ItemIsUserCheckable | Qt::ItemIsEnabled) : flags;
}

QVariant TableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole)
    {
        if (orientation == Qt::Horizontal) {
            switch (section)
            {
            case 0:
                return QString("Pulse");
            case 9:
                return QString("LP");
            default:
                return QString::number(section);
            }
        }else{
            return QString::number(section+1);
        }
    }
    return QVariant();
}

