#ifndef SMALLHUB_H
#define SMALLHUB_H

#include <QMainWindow>
#include <QTableView>
#include <QComboBox>
#include <QCheckBox>
#include "settingsdialog.h"
#include "mdevicesmallhub.h"
#include "tablemodel.h"

namespace Ui {
class SmallHub;
}

class SmallHub : public QMainWindow
{
    Q_OBJECT

public:
    explicit SmallHub(MDeviceSmallHub *_mDevice, QWidget *parent = 0);
    ~SmallHub();
    MDeviceSmallHub *mDevice;
    int mDeviceID;

private:
    void hideElements(QTableView* tableview, QString mode, int count, bool LP);
    void setModeDevice(int input, int count, QString mode, bool LP);
    void setStateWidgets(MDevice::InputMode mode, QList<QWidget*> list);
    void Init();

signals:
    void Close();

private slots:

    void on_comboBoxMode_1_currentIndexChanged(const QString &arg1);
    void on_comboBoxMode_2_currentIndexChanged(const QString &arg1);
    void on_comboBoxMode_3_currentIndexChanged(const QString &arg1);
    void on_comboBoxMode_4_currentIndexChanged(const QString &arg1);
    void on_comboBoxMode_5_currentIndexChanged(const QString &arg1);
    void on_comboBoxMode_6_currentIndexChanged(const QString &arg1);
    void on_comboBoxMode_7_currentIndexChanged(const QString &arg1);
    void on_comboBoxMode_8_currentIndexChanged(const QString &arg1);

    void on_pushButton_clicked();

    void on_pushButtonSave_clicked();

    void on_pushButtonBack_clicked();

    void hideColumnsModel();
    void getDeviceMode();

private:
    Ui::SmallHub *ui;

    SettingsDialog *m_settingsDialog;
    TableModel *mTableModel;
    QList<TableModel*> mTableModelList;
    QHash<int,QList<QWidget*>> mWidgetsList;

};

#endif // SMALLHUB_H
