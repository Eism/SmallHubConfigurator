#include "mdevicesmartexecutor.h"

#define DMXADD 10
#define DMXLVL 11

MDeviceSmartExecutor::MDeviceSmartExecutor(int _NumberCoilsRegisters, int _NumberDiscreteRegisters, int _NumberHoldingRegisters, int _NumberInputRegisters, QObject *parent):
    MDevice(_NumberCoilsRegisters, _NumberDiscreteRegisters, _NumberHoldingRegisters, _NumberInputRegisters, parent)
{

}

int MDeviceSmartExecutor::GetDMXAdress()
{
    return HoldingRegisters.value(DMXADD);
}

void MDeviceSmartExecutor::SetDMXAdress(int newValue)
{
    HoldingRegisters.setValue(DMXADD,newValue);
}

int MDeviceSmartExecutor::GetDMXLevevl()
{
    return HoldingRegisters.value(DMXLVL);
}

void MDeviceSmartExecutor::SetDMXLevel(int newValue)
{
    HoldingRegisters.setValue(DMXLVL,newValue);
}

void MDeviceSmartExecutor::SetSwitchValue(int newValue)
{
    CoilsRegisters.setValue(0, newValue);
    WriteToDevice();
}

int MDeviceSmartExecutor::GetSwitchValue()
{
    return CoilsRegisters.value(0);
}

void MDeviceSmartExecutor::SetDimmerValue(int newValue)
{
    HoldingRegisters.setValue(20, newValue);
    WriteToDevice();
}

int MDeviceSmartExecutor::GetDimmerValue()
{
    return HoldingRegisters.value(20);
}
