#ifndef DIMMERFORME_H
#define DIMMERFORME_H

#include <QMainWindow>

#include "mdevicesmartexecutor.h"

namespace Ui {
class DimmerForme;
}

class DimmerForme : public QMainWindow
{
    Q_OBJECT

public:
    explicit DimmerForme(MDeviceSmartExecutor* _mDevice, QWidget *parent = 0);
    ~DimmerForme();

    struct Settings {
        int parity = QSerialPort::NoParity;
        int baud = QSerialPort::Baud115200;
        int dataBits = QSerialPort::Data8;
        int stopBits = QSerialPort::OneStop;
        int responseTime = 200;
        int numberOfRetries = 3;
        int deviceMode = 2;
    };

private:
    void GetConnectionSettings();

signals:
    void Close();

private slots:
    void on_pushButtonSaveSettings_clicked();

    void on_lineEditDMXAdress_textChanged(const QString &arg1);

    void on_pushButtonUpdate_clicked();

    void on_pushButtonBack_clicked();

    void on_pushButtonSaveAdress_clicked();

    void on_horizontalSliderDimmer_sliderReleased();

    void UpdateDMXData();
    void SaveDMXData();

private:
    Ui::DimmerForme *ui;
    MDeviceSmartExecutor* mDevice;
    Settings mSettings;
    bool save;
};

#endif // DIMMERFORME_H
