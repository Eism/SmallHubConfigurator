#include "mdevicesmallhub.h"


MDeviceSmallHub::MDeviceSmallHub(int _NumberCoilsRegisters, int _NumberDiscreteRegisters, int _NumberHoldingRegisters, int _NumberInputRegisters, QObject *parent):
    MDevice(_NumberCoilsRegisters, _NumberDiscreteRegisters, _NumberHoldingRegisters, _NumberInputRegisters, parent)
{
}

void MDeviceSmallHub::ModeChange(int input, MDevice::InputMode mode)
{
    Q_ASSERT(input >= 0 && input < 8);
    HoldingRegisters.setValue(8 + input, (quint16)mode);
}

void MDeviceSmallHub::SetState(int input, int output, MDevice::Phases phase, bool state)
{
    if(phase == Pulse){
        quint16 mask = 1;
        mask = mask << (output);
        if(state)
        {
            HoldingRegisters.setValue(input, HoldingRegisters.value(input) | mask);
        }else{
            HoldingRegisters.setValue(input, ~(~HoldingRegisters.value(input) | mask));
        }
    }else if(phase == LongPush){
        quint16 mask = 1;
        mask = mask << (output );
        if(state)
        {
            HoldingRegisters.setValue(80 + input, HoldingRegisters.value(80 + input) | mask);
        }else{
            HoldingRegisters.setValue(80 + input, ~(~HoldingRegisters.value(80 + input) | mask));
        }
    }else{
        quint16 mask = 1;
        mask = mask << (output);
        if(state)
        {
            HoldingRegisters.setValue(16 + 8 * input + (phase - 1), HoldingRegisters.value(16 + 8 * input + (phase - 1)) | mask);
        }else{
            HoldingRegisters.setValue(16 + 8 * input + (phase - 1), ~(~HoldingRegisters.value(16 + 8 * input + (phase - 1)) | mask));
        }
        mask = 0;
        for(int i = 0; i <= HoldingRegisters.value(8 + input); i++)
        {
            mask = mask | HoldingRegisters.value(16 + 8 * input + i);
        }
        HoldingRegisters.setValue(input, mask);
    }
}

bool MDeviceSmallHub::GetState(int input, int output, MDevice::Phases phase)
{
    if(phase == Pulse){
        quint16 mask = 1;
        mask = mask << output;
        return (HoldingRegisters.value(input) & mask) >> output;
    }else if(phase == LongPush){
        quint16 mask = 1;
        mask = mask << (output);
        return (HoldingRegisters.value(80 + input) & mask) >> output;
    }else{
        quint16 mask = 1;
        mask = mask << output;
        return (HoldingRegisters.value(16 + 8 * input + (phase - 1)) & mask) >> output;
    }
}
