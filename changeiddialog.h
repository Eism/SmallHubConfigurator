#ifndef CHANGEIDDIALOG_H
#define CHANGEIDDIALOG_H

#include <QDialog>
#include "logindevice.h"

namespace Ui {
class ChangeIDDialog;
}

class ChangeIDDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ChangeIDDialog(QWidget *parent = 0);
    ~ChangeIDDialog();

private slots:
    void on_pushButtonOK_clicked();

private:
    Ui::ChangeIDDialog *ui;
    LoginDevice* mainWindow;
};

#endif // CHANGEIDDIALOG_H
