#ifndef MDEVICESMALLHUB_H
#define MDEVICESMALLHUB_H

#include "mdevice.h"

class MDeviceSmallHub : public MDevice
{
public:
    MDeviceSmallHub(int _NumberCoilsRegisters, int _NumberDiscreteRegisters, int _NumberHoldingRegisters, int _NumberInputRegisters, QObject *parent);

    void ModeChange(int input, InputMode mode);
    void SetState(int input, int output, Phases phase, bool state);
    bool GetState(int input, int output, MDevice::Phases phase);
};

#endif // MDEVICESMALLHUB_H
