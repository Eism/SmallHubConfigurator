#-------------------------------------------------
#
# Project created by QtCreator 2017-01-15T19:47:44
#
#-------------------------------------------------

QT       += core gui serialbus serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SmallHabConfigurator
TEMPLATE = app


SOURCES += main.cpp\
    mdevice.cpp \
    tablemodel.cpp \
    changeiddialog.cpp \
    logindevice.cpp \
    mdevicesmallhub.cpp \
    mdevicesmartexecutor.cpp \
    smallhub.cpp \
    relleforme.cpp \
    dimmerforme.cpp \
    settingsdialog.cpp

HEADERS  += \
    mdevice.h \
    tablemodel.h \
    changeiddialog.h \
    logindevice.h \
    mdevicesmallhub.h \
    mdevicesmartexecutor.h \
    smallhub.h \
    relleforme.h \
    dimmerforme.h \
    settingsdialog.h

FORMS    += \
    changeiddialog.ui \
    logindevice.ui \
    smallhub.ui \
    relleforme.ui \
    dimmerforme.ui \
    settingsdialog.ui

DISTFILES += \
    text

RESOURCES += \
    images.qrc
