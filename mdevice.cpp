#include "mdevice.h"
#include <QVariant>
#include <QMessageBox>
#include <QDebug>
#include <QTime>
#include <QCoreApplication>

MDevice::MDevice(int _NumberCoilsRegisters, int _NumberDiscreteRegisters, int _NumberHoldingRegisters, int _NumberInputRegisters, QObject *parent) : QObject(parent),
    NumberCoilsRegisters(_NumberCoilsRegisters),
    NumberDiscreteRegisters(_NumberDiscreteRegisters),
    NumberHoldingRegisters(_NumberHoldingRegisters),
    NumberInputRegisters(_NumberInputRegisters),
    CoilsRegisters(QModbusDataUnit::Coils, StartCoilsRegisters, _NumberCoilsRegisters),
    DiscreteRegisters(QModbusDataUnit::DiscreteInputs, StartDiscreteRegisters, _NumberDiscreteRegisters),
    HoldingRegisters(QModbusDataUnit::HoldingRegisters, StartHoldingRegisters, _NumberHoldingRegisters),
    InputRegisters(QModbusDataUnit::InputRegisters, StartInputRegisters, _NumberInputRegisters)
{
    SendState = Idle;
    connect(this, &MDevice::WriteFinished, this, &MDevice::SendSaveCommand);
}

void MDevice::ConnectToDevice(QString PortName, QSerialPort::Parity Parity,
                              QSerialPort::BaudRate Baud, QSerialPort::DataBits DataBits,
                              QSerialPort::StopBits StopBits, int ResponseTime, int NumberOfRetries)
{
    if(!ModbusDevice){
        ModbusDevice = new QModbusRtuSerialMaster(this);
    }

    if (ModbusDevice->state() != QModbusDevice::ConnectedState) {
        ModbusDevice->setConnectionParameter(QModbusDevice::SerialPortNameParameter,
                                             QVariant(PortName));
        ModbusDevice->setConnectionParameter(QModbusDevice::SerialParityParameter,
                                             QVariant(Parity));
        ModbusDevice->setConnectionParameter(QModbusDevice::SerialBaudRateParameter,
                                             QVariant(Baud));
        ModbusDevice->setConnectionParameter(QModbusDevice::SerialDataBitsParameter,
                                             QVariant(DataBits));
        ModbusDevice->setConnectionParameter(QModbusDevice::SerialStopBitsParameter,
                                             QVariant(StopBits));
        ModbusDevice->setTimeout(ResponseTime);
        ModbusDevice->setNumberOfRetries(NumberOfRetries);

        if (!ModbusDevice->connectDevice()) {
            QMessageBox msgBox;
            msgBox.setText(tr("Connect failed: ") + ModbusDevice->errorString());
            msgBox.exec();
        }else{
            connect(ModbusDevice, &QModbusClient::errorOccurred, [this](QModbusDevice::Error) {
                QMessageBox msgBox;
                msgBox.setText(tr("Connect failed: ") + ModbusDevice->errorString());
                msgBox.exec();
            });
        }

        connect(ModbusDevice, &QModbusClient::stateChanged,this, &MDevice::onStateChanged);
        emit ConnectStateChanged(ModbusDevice->state());
    } else {
        ModbusDevice->disconnectDevice();
        delete ModbusDevice;
        ModbusDevice = nullptr;
    }
}

MDevice::InputMode MDevice::GetMode(int input)
{
    Q_ASSERT(input >= 0 && input < 8);
    return static_cast<MDevice::InputMode>(HoldingRegisters.value(8 + input));
}

void MDevice::SetNumberRegister(int newNumberCoilsRegisters, int newNumberDiscreteRegisters, int newNumberHoldingRegisters, int newNumberInputRegisters)
{
    NumberCoilsRegisters = newNumberCoilsRegisters;
    NumberDiscreteRegisters = newNumberDiscreteRegisters;
    NumberHoldingRegisters = newNumberHoldingRegisters;
    NumberInputRegisters = newNumberInputRegisters;

    CoilsRegisters.setValues(QVector<quint16>(NumberCoilsRegisters));
    DiscreteRegisters.setValues(QVector<quint16>(NumberDiscreteRegisters));
    HoldingRegisters.setValues(QVector<quint16>(NumberHoldingRegisters));
    InputRegisters.setValues(QVector<quint16>(NumberInputRegisters));
}

void MDevice::SetConnectionSettings(int Parity, int Baud)
{
    HoldingRegisters.setValue(5, Parity);
    HoldingRegisters.setValue(3, Baud);
}

void MDevice::SetDeviceMode(int mode)
{
    HoldingRegisters.setValue(9, mode);
}

void MDevice::ReadDevice()
{
    QModbusReply* reply;
    QModbusDataUnit* HoldingRegistersCopy;

    switch (SendState) {
    case Idle:
        reply = ModbusDevice->sendReadRequest(CoilsRegisters, DeviceID);
        if(!reply->isFinished())
        {
            connect(reply, &QModbusReply::finished, this, &MDevice::readReady);
        }
        SendState = SendCoilsRegisters;
        emit TransferStatus(SendCoilsRegisters);
        break;
    case SendCoilsRegisters:
        reply = ModbusDevice->sendReadRequest(DiscreteRegisters, DeviceID);
        if(!reply->isFinished())
        {
            connect(reply, &QModbusReply::finished, this, &MDevice::readReady);
        }
        SendState = SendDiscreteRegisters;
        emit TransferStatus(SendDiscreteRegisters);
        break;
    case SendDiscreteRegisters:
    case SendHoldingRegisters:
        HoldingRegistersCopy = new QModbusDataUnit(QModbusDataUnit::HoldingRegisters, StartHoldingRegisters + packetNumber * 10, 10);
        reply = ModbusDevice->sendReadRequest(*HoldingRegistersCopy, DeviceID);
        if(!reply->isFinished())
        {
            connect(reply, &QModbusReply::finished, this, &MDevice::readReady);
        }
        SendState = SendHoldingRegisters;
        emit TransferStatus(SendHoldingRegisters);
        if(packetNumber >= NumberHoldingRegisters / 10){
            SendState = SendLastHoldingRegisters;
        }
        break;
    case SendLastHoldingRegisters:
        reply = ModbusDevice->sendReadRequest(InputRegisters, DeviceID);
        if(!reply->isFinished())
        {
            connect(reply, &QModbusReply::finished, this, &MDevice::readReady);
        }
        SendState = SendInputRegisters;
        emit TransferStatus(SendInputRegisters);
        break;
    case SendInputRegisters:
        emit ReadFinished();
        SendState = Idle;
        emit TransferStatus(Idle);
        break;
    default:
        break;
    }
}

void MDevice::WriteToDevice()
{
    QModbusReply* reply;
    QModbusDataUnit* HoldingRegistersCopy;
    switch (SendState) {
    case Idle:
        reply = ModbusDevice->sendWriteRequest(CoilsRegisters, DeviceID);
        if(!reply->isFinished())
        {
            connect(reply, &QModbusReply::finished, this, &MDevice::writeReady);
        }
        SendState = SendCoilsRegisters;
        emit TransferStatus(SendCoilsRegisters);
        break;
    case SendCoilsRegisters:
    case SendHoldingRegisters:
        HoldingRegistersCopy = new QModbusDataUnit(QModbusDataUnit::HoldingRegisters, StartHoldingRegisters + packetNumber * 10, 10);
        for(int i = 0; i < 10; i++)
        {
            HoldingRegistersCopy->setValue(i,HoldingRegisters.value(i + packetNumber * 10));
        }
        reply = ModbusDevice->sendWriteRequest(*HoldingRegistersCopy, DeviceID);
        SendState = SendHoldingRegisters;
        if(!reply->isFinished())
        {
            connect(reply, &QModbusReply::finished, this, &MDevice::writeReady);
        }
        emit TransferStatus(SendHoldingRegisters);
        if(packetNumber >= NumberHoldingRegisters / 10){
            SendState = SendLastHoldingRegisters;
        }
        break;
    case SendLastHoldingRegisters:
        if (save)
            emit WriteFinished();
        packetNumber = 0;
        SendState = Idle;
        emit TransferStatus(Idle);
        break;
    default:
        break;
    }
}

void MDevice::SaveOnDevice()
{
    save = true;
    WriteToDevice();
}

void MDevice::SetDeviceID(int ID)
{
    DeviceID = ID;
}

void MDevice::SendNewDeviceID(int ID, bool BoardCast)
{
    emit TransferStatus(SendHoldingRegisters);
    QModbusDataUnit HoldingID(QModbusDataUnit::HoldingRegisters,40000,1);
    HoldingID.setValue(0,ID);
    ModbusDevice->sendWriteRequest(HoldingID,BoardCast?0:DeviceID);
    SendSaveCommand();
}

void MDevice::SendSaveCommand()
{
    qDebug() << "send save";
    QModbusDataUnit CoilCommandSave(QModbusDataUnit::Coils, StartCoilsRegisters+8, 1);
    CoilCommandSave.setValue(0,1);
    ModbusDevice->sendWriteRequest(CoilCommandSave, 0);
    emit TransferStatus(Idle);
    save = false;
}

void MDevice::onStateChanged()
{
    emit ConnectStateChanged(ModbusDevice->state());
    switch (ModbusDevice->state()) {
    case QModbusDevice::UnconnectedState:
        qDebug()<<"QModbusDevice::UnconnectedState";
        break;
    case QModbusDevice::ClosingState:
        qDebug()<<"QModbusDevice::ClosingState";
        break;
    case QModbusDevice::ConnectedState:
        qDebug()<<"QModbusDevice::ConnectedState";
        break;
    default:
        break;
    }
}

void MDevice::readReady()
{
    auto reply = qobject_cast<QModbusReply *>(sender());
    if (!reply)
        return;
    QModbusDataUnit CopyHoldingRegisters;
    if (reply->error() == QModbusDevice::NoError) {
        switch (SendState) {
        case SendCoilsRegisters:
            CoilsRegisters = reply->result();
            ReadDevice();
            break;
        case SendDiscreteRegisters:
            DiscreteRegisters = reply->result();
            ReadDevice();
            break;
        case SendHoldingRegisters:
        case SendLastHoldingRegisters:
            CopyHoldingRegisters = reply->result();
            for(int i = 0; i < 10; i++)
            {
                HoldingRegisters.setValue(i + packetNumber * 10, CopyHoldingRegisters.value(i));
            }
            packetNumber++;
            ReadDevice();
            break;
        case SendInputRegisters:
            packetNumber = 0;
            InputRegisters = reply->result();
            ReadDevice();
            break;
        default:
            break;
        }
    } else {
        qDebug()<<reply->errorString();
        emit Error(reply->errorString());
        packetNumber = 0;
        SendState = Idle;
    }
    if(!ModbusDevice)
        return;
    reply->deleteLater();
}

void MDevice::writeReady()
{
    auto reply = qobject_cast<QModbusReply *>(sender());
    if (!reply)
        return;
    if (reply->error() == QModbusDevice::NoError) {
        switch (SendState) {
        case SendCoilsRegisters:
            WriteToDevice();
            break;
        case SendHoldingRegisters:
        case SendLastHoldingRegisters:
            packetNumber++;
            WriteToDevice();
            break;
        default:
            packetNumber = 0;
            SendState = Idle;
            break;
        }
    } else {
        qDebug()<<reply->errorString();
        emit Error(reply->errorString());
        packetNumber = 0;
        SendState = Idle;
    }
    if(!ModbusDevice)
        return;
    reply->deleteLater();
}

MDevice::~MDevice()
{
    if(ModbusDevice)
        ModbusDevice->disconnectDevice();

    delete ModbusDevice;
}
