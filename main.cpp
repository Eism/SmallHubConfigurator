#include "smallhub.h"
#include "logindevice.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("Org NAME");
    QCoreApplication::setOrganizationDomain("Org DOMAIN");
    QCoreApplication::setApplicationName("SmallHabConfiguratior");

    QApplication a(argc, argv);
    LoginDevice w;
    w.show();

    return a.exec();
}
