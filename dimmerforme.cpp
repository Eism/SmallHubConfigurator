#include "dimmerforme.h"
#include "ui_dimmerforme.h"

#include <QSettings>
#include <QDebug>
#include <QMessageBox>
#include <QTimer>

#define SETTINGS_FILE_NAME "settings.conf"
#define CONFIGURATION_CATEGORY QString("configuration")

DimmerForme::DimmerForme(MDeviceSmartExecutor *_mDevice, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DimmerForme)
{
    ui->setupUi(this);
    resize(100,100);
    setMaximumSize(width(),height());

    GetConnectionSettings();

    mDevice = _mDevice;
    ui->horizontalSliderDimmer->setValue(mDevice->GetDimmerValue());

    ui->lineEditDMXAdress->setText(QString::number(mDevice->GetDMXAdress()));
    ui->lineEditDMXAdress->setValidator(new QIntValidator(1,512,this));

    connect(mDevice,SIGNAL(ReadFinished()),this,SLOT(UpdateDMXData()));
    connect(mDevice,SIGNAL(WriteFinished()),this,SLOT(SaveDMXData()));

    mDevice->SetNumberRegister(16,1,21,2);
    QTimer::singleShot(100, [=]{
        mDevice->ReadDevice();
    });
}

DimmerForme::~DimmerForme()
{
    delete ui;
}

void DimmerForme::GetConnectionSettings()
{
    /*
     * Считывание настроек
    */
    QSettings *settings = new QSettings(SETTINGS_FILE_NAME,QSettings::IniFormat);
    settings->beginGroup(CONFIGURATION_CATEGORY);

    mSettings.parity = settings->value("parity").toInt();
    if (mSettings.parity > 0)
        ui->parityCombo->setCurrentIndex(mSettings.parity-1);
    else ui->parityCombo->setCurrentIndex(mSettings.parity);

    mSettings.baud = settings->value("baud",115200).toInt();
    ui->baudCombo->setCurrentIndex(ui->baudCombo->findText(QString::number(mSettings.baud)));

    mSettings.deviceMode = settings->value("deviceMode",2).toInt();
    ui->deviceModeCombo->setCurrentIndex(mSettings.deviceMode - 2);

    settings->endGroup();
}

void DimmerForme::UpdateDMXData()
{
    ui->pushButtonSaveAdress->setEnabled(true);
    ui->pushButtonSaveSettings->setEnabled(true);
    ui->pushButtonUpdate->setEnabled(true);

    ui->labelLog->setText("Данные прочитаны");
    ui->lineEditDMXAdress->setText(QString::number(mDevice->GetDMXAdress()));
    ui->horizontalSliderDimmer->setValue(mDevice->GetDimmerValue());
}

void DimmerForme::SaveDMXData()
{
    if (save)
    {
        QMessageBox* msgBox = new QMessageBox();
        msgBox->setText(tr("Настройки сохранены, устройство перезапущено. Пожалуйста переподключитесь с новыми настройками."));
        msgBox->addButton(tr("Да"), QMessageBox::YesRole);
        msgBox->exec();

        QTimer::singleShot(100, [=]{
            close();
            emit Close();
            return;
        });
    }

    ui->labelLog->setText("Данные сохранены");
    ui->lineEditDMXAdress->setText(QString::number(mDevice->GetDMXAdress()));
    UpdateDMXData();
}

void DimmerForme::on_pushButtonSaveSettings_clicked()
{
    if (!mDevice) return;
    mSettings.parity = ui->parityCombo->currentIndex();
    mSettings.baud = ui->baudCombo->currentIndex();
    mSettings.deviceMode = ui->deviceModeCombo->currentIndex() + 2;

    mDevice->SetConnectionSettings(mSettings.parity, mSettings.baud);
    mDevice->SetDeviceMode(mSettings.deviceMode);
    mDevice->SaveOnDevice();
    save = true;

    if (mSettings.parity > 0)
        mSettings.parity++;
    mSettings.baud = ui->baudCombo->currentText().toInt();

    QSettings *settings = new QSettings(SETTINGS_FILE_NAME,QSettings::IniFormat);
    settings->beginGroup(CONFIGURATION_CATEGORY);
    settings->setValue("parity", mSettings.parity);
    settings->setValue("baud", mSettings.baud);
    settings->setValue("deviceMode", mSettings.deviceMode);
    settings->endGroup();
    settings->sync();
}

void DimmerForme::on_pushButtonSaveAdress_clicked()
{
    mDevice->SaveOnDevice();
}

void DimmerForme::on_horizontalSliderDimmer_sliderReleased()
{
    mDevice->SetDimmerValue(ui->horizontalSliderDimmer->sliderPosition());
    qDebug() << "dimmer send " << ui->horizontalSliderDimmer->sliderPosition();
}

void DimmerForme::on_lineEditDMXAdress_textChanged(const QString &arg1)
{
    mDevice->SetDMXAdress(arg1.toInt());
}

void DimmerForme::on_pushButtonUpdate_clicked()
{
    mDevice->ReadDevice();
}

void DimmerForme::on_pushButtonBack_clicked()
{
    emit Close();
    close();
}
