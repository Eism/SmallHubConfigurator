#include "relleforme.h"
#include "ui_relleforme.h"

#include <QSettings>
#include <QDebug>
#include <QMessageBox>
#include <QTimer>

#define SETTINGS_FILE_NAME "settings.conf"
#define CONFIGURATION_CATEGORY QString("configuration")

RelleForme::RelleForme(MDeviceSmartExecutor *_mDevice, QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::RelleForme)
{
    ui->setupUi(this);
    resize(100,100);
    setMaximumSize(width(),height());

    GetConnectionSettings();

    mDevice = _mDevice;
    ui->horizontalSliderRelle->setValue(mDevice->GetSwitchValue());

    ui->lineEditDMXAdress->setText(QString::number(mDevice->GetDMXAdress()));
    ui->lineEditDMXAdress->setValidator(new QIntValidator(1,512,this));

    ui->lineEditDMXLevel->setText(QString::number(mDevice->GetDMXLevevl()));
    ui->lineEditDMXLevel->setValidator(new QIntValidator(1,512,this));

    connect(mDevice,SIGNAL(ReadFinished()),this,SLOT(UpdateDMXData()));
    connect(mDevice,SIGNAL(WriteFinished()),this,SLOT(SaveDMXData()));

    mDevice->SetNumberRegister(16,1,21,2);
    QTimer::singleShot(100, [=]{
        mDevice->ReadDevice();
    });

}

RelleForme::~RelleForme()
{
    delete ui;
}

void RelleForme::GetConnectionSettings()
{
    /*
     * Считывание настроек
    */
    QSettings *settings = new QSettings(SETTINGS_FILE_NAME,QSettings::IniFormat);
    settings->beginGroup(CONFIGURATION_CATEGORY);

    mSettings.parity = settings->value("parity").toInt();
    if (mSettings.parity > 0)
        ui->parityCombo->setCurrentIndex(mSettings.parity-1);
    else ui->parityCombo->setCurrentIndex(mSettings.parity);

    mSettings.baud = settings->value("baud",115200).toInt();
    ui->baudCombo->setCurrentIndex(ui->baudCombo->findText(QString::number(mSettings.baud)));

    mSettings.deviceMode = settings->value("deviceMode",2).toInt();
    ui->deviceModeCombo->setCurrentIndex(mSettings.deviceMode - 2);

    settings->endGroup();
}

void RelleForme::UpdateDMXData()
{
    ui->pushButtonSaveDeviceInfo->setEnabled(true);
    ui->pushButtonSaveSettings->setEnabled(true);
    ui->pushButtonUpdate->setEnabled(true);

    ui->labelLog->setText("Данные прочитаны");
    ui->lineEditDMXAdress->setText(QString::number(mDevice->GetDMXAdress()));
    ui->lineEditDMXLevel->setText(QString::number(mDevice->GetDMXLevevl()));
    ui->horizontalSliderRelle->setValue(mDevice->GetSwitchValue());
    qDebug() << "end update";
}

void RelleForme::SaveDMXData()
{
    if (save)
    {
        QMessageBox msgBox;
        msgBox.setText(tr("Настройки сохранены, устройство перезапущено. Пожалуйста переподключитесь с новыми настройками."));
        msgBox.addButton(tr("Да"), QMessageBox::YesRole);
        msgBox.exec();
        QTimer::singleShot(100, [=]{
            close();
            emit Close();
            return;
        });
    }

    ui->labelLog->setText("Данные сохранены");
    ui->lineEditDMXAdress->setText(QString::number(mDevice->GetDMXAdress()));
    ui->lineEditDMXLevel->setText(QString::number(mDevice->GetDMXLevevl()));
    UpdateDMXData();
}

void RelleForme::on_pushButtonSaveSettings_clicked()
{
    if (!mDevice) return;
    mSettings.parity = ui->parityCombo->currentIndex();
    mSettings.baud = ui->baudCombo->currentIndex();
    mSettings.deviceMode = ui->deviceModeCombo->currentIndex() + 2;

    mDevice->SetConnectionSettings(mSettings.parity, mSettings.baud);
    mDevice->SetDeviceMode(mSettings.deviceMode);
    mDevice->SaveOnDevice();
    save = true;

    if (mSettings.parity > 0)
        mSettings.parity++;
    mSettings.baud = ui->baudCombo->currentText().toInt();

    QSettings *settings = new QSettings(SETTINGS_FILE_NAME,QSettings::IniFormat);
    settings->beginGroup(CONFIGURATION_CATEGORY);
    settings->setValue("parity", mSettings.parity);
    settings->setValue("baud", mSettings.baud);
    settings->setValue("deviceMode", mSettings.deviceMode);
    settings->endGroup();
    settings->sync();
}

void RelleForme::on_pushButtonSaveDeviceInfo_clicked()
{
    mDevice->SaveOnDevice();
}

void RelleForme::on_horizontalSliderRelle_sliderReleased()
{
    mDevice->SetSwitchValue(ui->horizontalSliderRelle->sliderPosition());
    qDebug() << "relle send " << ui->horizontalSliderRelle->sliderPosition();
}

void RelleForme::on_lineEditDMXAdress_textChanged(const QString &arg1)
{
    mDevice->SetDMXAdress(arg1.toInt());
}

void RelleForme::on_lineEditDMXLevel_textChanged(const QString &arg1)
{
    mDevice->SetDMXLevel(arg1.toInt());
}

void RelleForme::on_pushButtonUpdate_clicked()
{
    mDevice->ReadDevice();
}

void RelleForme::on_pushButtonBack_clicked()
{
    emit Close();
    close();
}
