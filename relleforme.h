#ifndef RELLEFORME_H
#define RELLEFORME_H

#include <QMainWindow>

#include "mdevicesmartexecutor.h"

namespace Ui {
class RelleForme;
}

class RelleForme : public QMainWindow
{
    Q_OBJECT

public:
    explicit RelleForme(MDeviceSmartExecutor* _mDevice, QWidget *parent = 0);
    ~RelleForme();

    struct Settings {
        int parity = QSerialPort::NoParity;
        int baud = QSerialPort::Baud115200;
        int dataBits = QSerialPort::Data8;
        int stopBits = QSerialPort::OneStop;
        int responseTime = 200;
        int numberOfRetries = 3;
        int deviceMode = 2;
    };

private:
    void GetConnectionSettings();

signals:
    void Close();

private slots:
    void on_pushButtonSaveSettings_clicked();

    void on_pushButtonSaveDeviceInfo_clicked();

    void on_horizontalSliderRelle_sliderReleased();

    void on_lineEditDMXAdress_textChanged(const QString &arg1);

    void on_lineEditDMXLevel_textChanged(const QString &arg1);

    void on_pushButtonUpdate_clicked();

    void on_pushButtonBack_clicked();

    void UpdateDMXData();
    void SaveDMXData();

private:
    Ui::RelleForme *ui;
    MDeviceSmartExecutor* mDevice;
    Settings mSettings;
    bool save;

};

#endif // RELLEFORME_H
