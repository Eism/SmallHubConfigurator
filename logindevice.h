#ifndef LOGINDEVICE_H
#define LOGINDEVICE_H

#include <QMainWindow>
#include "settingsdialog.h"
#include "mdevicesmallhub.h"
#include "smallhub.h"

namespace Ui {
class LoginDevice;
}

class LoginDevice : public QMainWindow
{
    Q_OBJECT

public:
    explicit LoginDevice(QWidget *parent = 0);
    ~LoginDevice();

    MDevice *mDevice;
    int mDeviceID;
private:
    void GetConnectionSettings();

private slots:
    void on_pushButtonConnect_clicked();
    void getDeviceData();
    void on_pushButtonConnect_2_clicked();
    void SetNewConnectionSettings(SettingsDialog::Settings settings);

private:
    Ui::LoginDevice *ui;
    SettingsDialog::Settings MSettings;
};

#endif // LOGINDEVICE_H
