#include "changeiddialog.h"
#include "ui_changeiddialog.h"

ChangeIDDialog::ChangeIDDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChangeIDDialog)
{
    ui->setupUi(this);
    resize(100,100);
    setMaximumSize(width(),height());
    mainWindow = dynamic_cast<LoginDevice*>(parent);

    connect(ui->checkBoxBC,&QCheckBox::stateChanged,[this](int arg1){ui->lineEditID->setEnabled(!arg1);});
    ui->lineEditID->setValidator(new QIntValidator(1,248,this));
    ui->lineEditNewID->setValidator(new QIntValidator(1,248,this));
}

ChangeIDDialog::~ChangeIDDialog()
{
    delete ui;
}

void ChangeIDDialog::on_pushButtonOK_clicked()
{
    mainWindow->mDevice->SetDeviceID(ui->lineEditID->text().toInt());

    int newID = ui->lineEditNewID->text().toInt();
    mainWindow->mDevice->SendNewDeviceID(newID, ui->checkBoxBC->isChecked());

    if (mainWindow->mDeviceID != newID)
    {
        mainWindow->mDevice->SetDeviceID(mainWindow->mDeviceID);
    }

    this->close();
}
