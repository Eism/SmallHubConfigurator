#include "smallhub.h"
#include "changeiddialog.h"
#include "ui_smallhub.h"
#include <QMessageBox>
#include <QDebug>

SmallHub::SmallHub(MDeviceSmallHub *_mDevice, QWidget *parent):
    QMainWindow(parent),
    ui(new Ui::SmallHub)
{
    ui->setupUi(this);

    mDevice = _mDevice;
    mDevice->SetNumberRegister(9, 7, 88, 7);

    connect(mDevice,&MDeviceSmallHub::TransferStatus,[this](MDeviceSmallHub::MSendState state){
        switch (state) {
        case MDeviceSmallHub::Idle:
            ui->labelLog->setText("Idle");
            break;
        case MDeviceSmallHub::SendCoilsRegisters:
            ui->labelLog->setText("Send Coils Registers");
            break;
        case MDeviceSmallHub::SendDiscreteRegisters:
            ui->labelLog->setText("Send Discrete Registers");
            break;
        case MDeviceSmallHub::SendHoldingRegisters:
            ui->labelLog->setText("Send Holding Registers");
            break;
        case MDeviceSmallHub::SendInputRegisters:
            ui->labelLog->setText("Send Input Registers");
            break;
        default:
            break;
        }
    });

    connect(mDevice,&MDeviceSmallHub::Error,[this](QString errStr){
        QMessageBox msgBox("Error",errStr,QMessageBox::Critical,0,0,0,this);
        msgBox.exec();
    });

    ui->pushButton->setEnabled(true);
    ui->pushButtonSave->setEnabled(true);
    ui->actionChange_ID->setEnabled(true);

    connect(mDevice, &MDeviceSmallHub::ConnectStateChanged,[this](QModbusDevice::State state){
        switch (state) {
        case QModbusDevice::ConnectedState:
            ui->pushButton->setEnabled(true);
            ui->pushButtonSave->setEnabled(true);
            ui->actionChange_ID->setEnabled(true);
            break;
        default:
            ui->pushButton->setEnabled(false);
            ui->pushButtonSave->setEnabled(false);
            ui->actionChange_ID->setEnabled(false);
            break;
        }
    });

    Init();
}

SmallHub::~SmallHub()
{
    delete ui;
}

void SmallHub::hideElements(QTableView* tableview, QString mode, int count, bool LP){
    if (mode == "Pulse"){
        for(int i=1;i<9;i++)
            tableview->hideColumn(i);

        tableview->showColumn(0);
        if (LP)
            tableview->showColumn(9);
        else
            tableview->hideColumn(9);
    }else {
        tableview->hideColumn(0);
        for(int i=1;i<=count;i++)
            tableview->showColumn(i);

        for(int i=count+1;i<9;i++)
            tableview->hideColumn(i);

        if (LP)
            tableview->showColumn(9);
        else
            tableview->hideColumn(9);
    }
}

void SmallHub::setModeDevice(int input,int count, QString mode, bool LP)
{
    if (mode == "Switch"){
        switch (count) {////// нумерация с 0?
        case 1:
            if (LP) mDevice->ModeChange(input,MDeviceSmallHub::LPhase1Mode);
            else mDevice->ModeChange(input,MDeviceSmallHub::Phase1Mode);
            break;
        case 2:
            if (LP) mDevice->ModeChange(input,MDeviceSmallHub::LPhase2Mode);
            else mDevice->ModeChange(input,MDeviceSmallHub::Phase2Mode);
            break;
        case 3:
            if (LP) mDevice->ModeChange(input,MDeviceSmallHub::LPhase3Mode);
            else mDevice->ModeChange(input,MDeviceSmallHub::Phase3Mode);
            break;
        case 4:
            if (LP) mDevice->ModeChange(input,MDeviceSmallHub::LPhase4Mode);
            else mDevice->ModeChange(input,MDeviceSmallHub::Phase4Mode);
            break;
        case 5:
            if (LP) mDevice->ModeChange(input,MDeviceSmallHub::LPhase5Mode);
            else mDevice->ModeChange(input,MDeviceSmallHub::Phase5Mode);
            break;
        case 6:
            if (LP) mDevice->ModeChange(input,MDeviceSmallHub::LPhase6Mode);
            else mDevice->ModeChange(input,MDeviceSmallHub::Phase6Mode);
            break;
        case 7:
            if (LP) mDevice->ModeChange(input,MDeviceSmallHub::LPhase7Mode);
            else mDevice->ModeChange(input,MDeviceSmallHub::Phase7Mode);
            break;
        case 8:
            if (LP) mDevice->ModeChange(input,MDeviceSmallHub::LPhase8Mode);
            else mDevice->ModeChange(input,MDeviceSmallHub::Phase8Mode);
            break;
        default:
            break;
        }
    }else if (mode=="Pulse"){
        if (LP) mDevice->ModeChange(input,MDeviceSmallHub::LPulseMode);
        else mDevice->ModeChange(input,MDeviceSmallHub::PulseMode);
    }
}

void SmallHub::hideColumnsModel()
{
    int count;
    QString mode;
    bool LP;
    switch (ui->tabWidget->currentIndex()) {
    case 0:
        mode = ui->comboBoxMode_1->currentText();
        count = ui->comboBoxPhase_1->currentIndex()+1;
        LP = ui->checkBoxLP_1->isChecked();
        setModeDevice(0,count,mode,LP);
        hideElements(ui->tableView_1,mode,count,LP);
        break;
    case 1:
        mode = ui->comboBoxMode_2->currentText();
        count = ui->comboBoxPhase_2->currentIndex()+1;
        LP = ui->checkBoxLP_2->isChecked();
        setModeDevice(1,count,mode,LP);
        hideElements(ui->tableView_2,mode,count,LP);
        break;
    case 2:
        mode = ui->comboBoxMode_3->currentText();
        count = ui->comboBoxPhase_3->currentIndex()+1;
        LP = ui->checkBoxLP_3->isChecked();
        setModeDevice(2,count,mode,LP);
        hideElements(ui->tableView_3,mode,count,LP);
        break;
    case 3:
        mode = ui->comboBoxMode_4->currentText();
        count = ui->comboBoxPhase_4->currentIndex()+1;
        LP = ui->checkBoxLP_4->isChecked();
        setModeDevice(3,count,mode,LP);
        hideElements(ui->tableView_4,mode,count,LP);
        break;
    case 4:
        mode = ui->comboBoxMode_5->currentText();
        count = ui->comboBoxPhase_5->currentIndex()+1;
        LP = ui->checkBoxLP_5->isChecked();
        setModeDevice(4,count,mode,LP);
        hideElements(ui->tableView_5,mode,count,LP);
        break;
    case 5:
        mode = ui->comboBoxMode_6->currentText();
        count = ui->comboBoxPhase_6->currentIndex()+1;
        LP = ui->checkBoxLP_6->isChecked();
        setModeDevice(5,count,mode,LP);
        hideElements(ui->tableView_6,mode,count,LP);
        break;
    case 6:
        mode = ui->comboBoxMode_7->currentText();
        count = ui->comboBoxPhase_7->currentIndex()+1;
        LP = ui->checkBoxLP_7->isChecked();
        setModeDevice(6,count,mode,LP);
        hideElements(ui->tableView_7,mode,count,LP);
        break;
    case 7:
        mode = ui->comboBoxMode_8->currentText();
        count = ui->comboBoxPhase_8->currentIndex()+1;
        LP = ui->checkBoxLP_8->isChecked();
        setModeDevice(7,count,mode,LP);
        hideElements(ui->tableView_8,mode,count,LP);
        break;
    default:
        break;
    }

}

void SmallHub::on_comboBoxMode_1_currentIndexChanged(const QString &arg1)
{

    if (arg1 == "Pulse"){
        ui->comboBoxPhase_1->hide();

    }else if (arg1 == "Switch"){
        ui->comboBoxPhase_1->show();
    }
    hideColumnsModel();

}

void SmallHub::on_comboBoxMode_2_currentIndexChanged(const QString &arg1)
{
    if (arg1 == "Pulse"){
        ui->comboBoxPhase_2->hide();

    }else if (arg1 == "Switch"){
        ui->comboBoxPhase_2->show();
    }

    hideColumnsModel();
}

void SmallHub::on_comboBoxMode_3_currentIndexChanged(const QString &arg1)
{
    if (arg1 == "Pulse"){
        ui->comboBoxPhase_3->hide();

    }else if (arg1 == "Switch"){
        ui->comboBoxPhase_3->show();
    }

    hideColumnsModel();
}

void SmallHub::on_comboBoxMode_4_currentIndexChanged(const QString &arg1)
{
    if (arg1 == "Pulse"){
        ui->comboBoxPhase_4->hide();

    }else if (arg1 == "Switch"){
        ui->comboBoxPhase_4->show();
    }

    hideColumnsModel();
}

void SmallHub::on_comboBoxMode_5_currentIndexChanged(const QString &arg1)
{
    if (arg1 == "Pulse"){
        ui->comboBoxPhase_5->hide();

    }else if (arg1 == "Switch"){
        ui->comboBoxPhase_5->show();
    }

    hideColumnsModel();
}

void SmallHub::on_comboBoxMode_6_currentIndexChanged(const QString &arg1)
{
    if (arg1 == "Pulse"){
        ui->comboBoxPhase_6->hide();

    }else if (arg1 == "Switch"){
        ui->comboBoxPhase_6->show();
    }

    hideColumnsModel();
}

void SmallHub::on_comboBoxMode_7_currentIndexChanged(const QString &arg1)
{
    if (arg1 == "Pulse"){
        ui->comboBoxPhase_7->hide();

    }else if (arg1 == "Switch"){
        ui->comboBoxPhase_7->show();
    }

    hideColumnsModel();
}

void SmallHub::on_comboBoxMode_8_currentIndexChanged(const QString &arg1)
{
    if (arg1 == "Pulse"){
        ui->comboBoxPhase_8->hide();

    }else if (arg1 == "Switch"){
        ui->comboBoxPhase_8->show();
    }

    hideColumnsModel();
}

void SmallHub::on_pushButton_clicked()
{
    mDevice->ReadDevice();
    connect(mDevice,SIGNAL(ReadFinished()),this,SLOT(getDeviceMode()));
}

void SmallHub::on_pushButtonSave_clicked()
{
    mDevice->SetDeviceID(mDevice->GetDeviceID());
    mDevice->SaveOnDevice();
}

void SmallHub::setStateWidgets(MDeviceSmallHub::InputMode mode, QList<QWidget *> list){

    if(mode == MDeviceSmallHub::PulseMode || mode == MDeviceSmallHub::LPulseMode){
        QComboBox *cBM = static_cast<QComboBox*>(list.at(0));
        cBM->setCurrentText("Pulse");
        if (mode == MDeviceSmallHub::PulseMode){
            QCheckBox *cBM = static_cast<QCheckBox*>(list.at(2));
            cBM->setChecked(false);
        }else{
            QCheckBox *cBM = static_cast<QCheckBox*>(list.at(2));
            cBM->setChecked(true);
        }
    }else if(static_cast<int>(mode) >= static_cast<int>(MDeviceSmallHub::Phase1Mode) &&
             static_cast<int>(mode) <= static_cast<int>(MDeviceSmallHub::Phase8Mode)){
        int iMode = static_cast<int>(mode);
        iMode--;
        QComboBox *cBM = static_cast<QComboBox*>(list.at(0));
        cBM->setCurrentText("Switch");
        QComboBox *cBP = static_cast<QComboBox*>(list.at(1));
        cBP->setCurrentIndex(iMode);
        QCheckBox *cBLP = static_cast<QCheckBox*>(list.at(2));
        cBLP->setChecked(false);
    }else{
        int iMode = static_cast<int>(mode)-8;
        iMode--;
        QComboBox *cBM = static_cast<QComboBox*>(list.at(0));
        cBM->setCurrentText("Switch");
        QComboBox *cBP = static_cast<QComboBox*>(list.at(1));
        cBP->setCurrentIndex(iMode-1);
        QCheckBox *cBLP = static_cast<QCheckBox*>(list.at(2));
        cBLP->setChecked(true);
        //LP
    }
}

void SmallHub::Init()
{
    for(int i=0;i<8;i++)
    {
        TableModel* tM=new TableModel(8,10,i);
        tM->setDevice(mDevice);
        mTableModelList.push_back(tM);
    }

    QList<QWidget*> list;
    list <<ui->comboBoxMode_1<<ui->comboBoxPhase_1<<ui->checkBoxLP_1;
    mWidgetsList[0] = list;

    list.clear();
    list <<ui->comboBoxMode_2<<ui->comboBoxPhase_2<<ui->checkBoxLP_2;
    mWidgetsList[1] = list;

    list.clear();
    list <<ui->comboBoxMode_3<<ui->comboBoxPhase_3<<ui->checkBoxLP_3;
    mWidgetsList[2] = list;

    list.clear();
    list <<ui->comboBoxMode_4<<ui->comboBoxPhase_4<<ui->checkBoxLP_4;
    mWidgetsList[3] = list;

    list.clear();
    list <<ui->comboBoxMode_5<<ui->comboBoxPhase_5<<ui->checkBoxLP_5;
    mWidgetsList[4] = list;

    list.clear();
    list <<ui->comboBoxMode_6<<ui->comboBoxPhase_6<<ui->checkBoxLP_6;
    mWidgetsList[5] = list;

    list.clear();
    list <<ui->comboBoxMode_7<<ui->comboBoxPhase_7<<ui->checkBoxLP_7;
    mWidgetsList[6] = list;

    list.clear();
    list <<ui->comboBoxMode_8<<ui->comboBoxPhase_8<<ui->checkBoxLP_8;
    mWidgetsList[7] = list;

    ui->tableView_1->setModel(mTableModelList.at(0));
    ui->tableView_2->setModel(mTableModelList.at(1));
    ui->tableView_3->setModel(mTableModelList.at(2));
    ui->tableView_4->setModel(mTableModelList.at(3));
    ui->tableView_5->setModel(mTableModelList.at(4));
    ui->tableView_6->setModel(mTableModelList.at(5));
    ui->tableView_7->setModel(mTableModelList.at(6));
    ui->tableView_8->setModel(mTableModelList.at(7));

    connect(ui->comboBoxPhase_1,SIGNAL(currentIndexChanged(int)),this,SLOT(hideColumnsModel()));
    connect(ui->comboBoxPhase_2,SIGNAL(currentIndexChanged(int)),this,SLOT(hideColumnsModel()));
    connect(ui->comboBoxPhase_3,SIGNAL(currentIndexChanged(int)),this,SLOT(hideColumnsModel()));
    connect(ui->comboBoxPhase_4,SIGNAL(currentIndexChanged(int)),this,SLOT(hideColumnsModel()));
    connect(ui->comboBoxPhase_5,SIGNAL(currentIndexChanged(int)),this,SLOT(hideColumnsModel()));
    connect(ui->comboBoxPhase_6,SIGNAL(currentIndexChanged(int)),this,SLOT(hideColumnsModel()));
    connect(ui->comboBoxPhase_7,SIGNAL(currentIndexChanged(int)),this,SLOT(hideColumnsModel()));
    connect(ui->comboBoxPhase_8,SIGNAL(currentIndexChanged(int)),this,SLOT(hideColumnsModel()));

    connect(ui->checkBoxLP_1,SIGNAL(stateChanged(int)),this,SLOT(hideColumnsModel()));
    connect(ui->checkBoxLP_2,SIGNAL(stateChanged(int)),this,SLOT(hideColumnsModel()));
    connect(ui->checkBoxLP_3,SIGNAL(stateChanged(int)),this,SLOT(hideColumnsModel()));
    connect(ui->checkBoxLP_4,SIGNAL(stateChanged(int)),this,SLOT(hideColumnsModel()));
    connect(ui->checkBoxLP_5,SIGNAL(stateChanged(int)),this,SLOT(hideColumnsModel()));
    connect(ui->checkBoxLP_6,SIGNAL(stateChanged(int)),this,SLOT(hideColumnsModel()));
    connect(ui->checkBoxLP_7,SIGNAL(stateChanged(int)),this,SLOT(hideColumnsModel()));
    connect(ui->checkBoxLP_8,SIGNAL(stateChanged(int)),this,SLOT(hideColumnsModel()));

    connect(ui->tabWidget,SIGNAL(currentChanged(int)),this,SLOT(hideColumnsModel()));
    hideColumnsModel();

    m_settingsDialog = new SettingsDialog(true,mDevice,this);

    connect(ui->actionOptions, &QAction::triggered, m_settingsDialog, &QDialog::show);
    connect(ui->actionChange_ID, &QAction::triggered, new ChangeIDDialog(this), &QDialog::show);

}

void SmallHub::getDeviceMode()
{
    ui->labelLog->setText("Данные прочитаны");

    for(int i=0;i<8;i++){
        setStateWidgets(mDevice->GetMode(i),mWidgetsList.value(i));
    }
}

void SmallHub::on_pushButtonBack_clicked()
{
    this->close();
    emit Close();
}
