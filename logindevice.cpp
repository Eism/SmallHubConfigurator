#include "logindevice.h"
#include "ui_logindevice.h"
#include <QMessageBox>
#include <QDebug>
#include <QSettings>
#include <QTimer>

#include "changeiddialog.h"
#include "relleforme.h"
#include "dimmerforme.h"

#define SETTINGS_FILE_NAME "settings.conf"
#define CONFIGURATION_CATEGORY QString("configuration")

LoginDevice::LoginDevice(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::LoginDevice)
{
    ui->setupUi(this);
    resize(100,100);
    setMaximumSize(width(),height());
    // 9 7 88 7 small
    // 8 1 12 2 smart
    // 8 1 10 2 all
    mDevice = new MDevice(16,1,10,2,this);

    connect(mDevice,&MDevice::Error,[this](QString errStr){
        ui->actionChange_ID->setEnabled(false);
        ui->pushButtonConnect->setEnabled(false);
        QMessageBox msgBox("Error",errStr,QMessageBox::Critical,0,0,0,this);
        msgBox.exec();
        QTimer::singleShot(100, [=]{
            mDevice->DisconnectDevice();
        });
    });

    ui->actionChange_ID->setEnabled(false);
    ui->lineEditDeviceId->setValidator(new QIntValidator(1,248,this));
    ui->pushButtonConnect->setEnabled(false);

    connect(ui->actionChange_ID, &QAction::triggered, new ChangeIDDialog(this), &QDialog::show);

    connect(mDevice,SIGNAL(ReadFinished()),this,SLOT(getDeviceData()));

    GetConnectionSettings();

    connect(mDevice, &MDevice::ConnectStateChanged,[this](QModbusDevice::State state){
        switch (state) {
        case QModbusDevice::ConnectedState:
        {
            ui->labelLog->setText("Подключен");
            ui->actionChange_ID->setEnabled(true);
            ui->pushButtonConnect->setEnabled(true);
            break;
        }
        default:
        {
            ui->actionChange_ID->setEnabled(false);
            break;
        }
        }
    });

    connect(mDevice,&MDevice::TransferStatus,[this](MDevice::MSendState state){
        switch (state) {
        case MDevice::Idle:
            ui->labelLog->setText("Idle");
            break;
        case MDevice::SendCoilsRegisters:
            ui->labelLog->setText("Send Coils Registers");
            break;
        case MDevice::SendDiscreteRegisters:
            ui->labelLog->setText("Send Discrete Registers");
            break;
        case MDevice::SendHoldingRegisters:
            ui->labelLog->setText("Send Holding Registers");
            break;
        case MDevice::SendInputRegisters:
            ui->labelLog->setText("Send Input Registers");
            break;
        default:
            break;
        }
    });

}

LoginDevice::~LoginDevice()
{
    delete mDevice;
    delete ui;
}

void LoginDevice::on_pushButtonConnect_clicked()
{
    mDeviceID = ui->lineEditDeviceId->text().toInt();
    mDevice->SetDeviceID(mDeviceID);
    mDevice->ReadDevice();
}

void LoginDevice::getDeviceData()
{
    int deviceVersion = mDevice->GetDeviceVersion();
    int hardwareVersion = mDevice->GetHardwareVersion();

    if (deviceVersion == 2 && hardwareVersion == 1)
    {
        disconnect(mDevice,SIGNAL(ReadFinished()),this,SLOT(getDeviceData()));
        this->close();

        RelleForme* rFm = new RelleForme((MDeviceSmartExecutor*) mDevice);
        rFm->setAttribute(Qt::WA_DeleteOnClose);
        rFm->show();

        connect(rFm, &RelleForme::Close, [this](){
            connect(mDevice,SIGNAL(ReadFinished()),this,SLOT(getDeviceData()));
            GetConnectionSettings();
            mDevice->DisconnectDevice();
            ui->pushButtonConnect->setEnabled(false);
            this->show();
        });
    }else
        if (deviceVersion == 3 && hardwareVersion == 1)
        {
            disconnect(mDevice,SIGNAL(ReadFinished()),this,SLOT(getDeviceData()));
            this->close();

            DimmerForme* dFm = new DimmerForme((MDeviceSmartExecutor*) mDevice);
            dFm->setAttribute(Qt::WA_DeleteOnClose);
            dFm->show();

            connect(dFm, &DimmerForme::Close, [this](){
                connect(mDevice,SIGNAL(ReadFinished()),this,SLOT(getDeviceData()));
                GetConnectionSettings();
                mDevice->DisconnectDevice();
                ui->pushButtonConnect->setEnabled(false);
                this->show();
            });
        }else
            if (deviceVersion == 1 && hardwareVersion == 1)
            {
                disconnect(mDevice,SIGNAL(ReadFinished()),this,SLOT(getDeviceData()));
                this->hide();
                //small
                SmallHub* sHb = new SmallHub((MDeviceSmallHub*)mDevice);
                sHb->setAttribute(Qt::WA_DeleteOnClose);
                sHb->show();

                connect(sHb,&SmallHub::Close,[this](){
                    connect(mDevice,SIGNAL(ReadFinished()),this,SLOT(getDeviceData()));
                    GetConnectionSettings();
                    mDevice->DisconnectDevice();
                    ui->pushButtonConnect->setEnabled(false);
                    this->show();
                });
            }
}

void LoginDevice::GetConnectionSettings()
{
    /*
     * Считывание настроек
    */
    QSettings *settings = new QSettings(SETTINGS_FILE_NAME,QSettings::IniFormat);
    settings->beginGroup(CONFIGURATION_CATEGORY);

    MSettings.parity = settings->value("parity").toInt();
    if (MSettings.parity > 0){
        ui->parityCombo->setCurrentIndex(MSettings.parity-1);
    }
    else ui->parityCombo->setCurrentIndex(MSettings.parity);

    MSettings.baud = settings->value("baud",115200).toInt();
    ui->baudCombo->setCurrentIndex(ui->baudCombo->findText(QString::number(MSettings.baud)));

    ui->lineEditSerialPort->setText(settings->value("port","COM4").toString());
    ui->lineEditDeviceId->setText(settings->value("adress","10").toString());
    settings->endGroup();
}

void LoginDevice::on_pushButtonConnect_2_clicked()
{
    if (!mDevice) return;
    MSettings.parity = ui->parityCombo->currentIndex();
    if (MSettings.parity > 0)
        MSettings.parity++;
    MSettings.baud = ui->baudCombo->currentText().toInt();

    QSettings *settings = new QSettings(SETTINGS_FILE_NAME,QSettings::IniFormat);
    settings->beginGroup(CONFIGURATION_CATEGORY);
    settings->setValue("parity", MSettings.parity);
    settings->setValue("baud", MSettings.baud);
    settings->setValue("port", ui->lineEditSerialPort->text());
    settings->setValue("adress",ui->lineEditDeviceId->text());
    settings->endGroup();
    settings->sync();

    mDevice->ConnectToDevice(ui->lineEditSerialPort->text(),
                             static_cast<QSerialPort::Parity>(MSettings.parity),
                             static_cast<QSerialPort::BaudRate>(MSettings.baud),
                             static_cast<QSerialPort::DataBits>(MSettings.dataBits),
                             static_cast<QSerialPort::StopBits>(MSettings.stopBits),
                             MSettings.responseTime,
                             MSettings.numberOfRetries);

    mDeviceID = ui->lineEditDeviceId->text().toInt();
    mDevice->SetDeviceID(mDeviceID);
}

void LoginDevice::SetNewConnectionSettings(SettingsDialog::Settings settings)
{
    MSettings = settings;
}
