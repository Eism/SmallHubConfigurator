#ifndef MDEVICESMARTEXECUTOR_H
#define MDEVICESMARTEXECUTOR_H

#include "mdevice.h"

class MDeviceSmartExecutor : public MDevice
{
public:
    MDeviceSmartExecutor(int _NumberCoilsRegisters, int _NumberDiscreteRegisters, int _NumberHoldingRegisters, int _NumberInputRegisters, QObject *parent = 0);

    int GetDMXAdress();
    void SetDMXAdress(int newValue);
    int GetDMXLevevl();
    void SetDMXLevel(int newValue);

    void SetSwitchValue(int newValue);
    int GetSwitchValue();
    void SetDimmerValue(int newValue);
    int GetDimmerValue();
};

#endif // MDEVICESMARTEXECUTOR_H
