#ifndef MDEVICE_H
#define MDEVICE_H

#include <QObject>
#include <QBitArray>
#include <QVector>
#include <QModbusClient>
#include <QModbusRtuSerialMaster>
#include <QSerialPort>
#include <QString>

#define REG_DEVICE_VERSION    0
#define REG_HARDWARE_VERSION 1

class MDevice : public QObject
{
    Q_OBJECT
public:
    MDevice(int _NumberCoilsRegisters,
            int _NumberDiscreteRegisters,
            int _NumberHoldingRegisters,
            int _NumberInputRegisters,
            QObject *parent = 0);

    enum InputMode {
        PulseMode = 0, Phase1Mode, Phase2Mode, Phase3Mode, Phase4Mode, Phase5Mode, Phase6Mode, Phase7Mode, Phase8Mode,
        LPulseMode, LPhase1Mode, LPhase2Mode, LPhase3Mode, LPhase4Mode, LPhase5Mode, LPhase6Mode, LPhase7Mode, LPhase8Mode
    };
    Q_ENUM(InputMode)

    enum Phases {
        Pulse = 0, Phase1, Phase2, Phase3, Phase4, Phase5, Phase6, Phase7, Phase8, LongPush
    };
    Q_ENUM(Phases)

    enum MSendState{
        Idle,
        SendCoilsRegisters,
        SendDiscreteRegisters,
        SendHoldingRegisters,
        SendLastHoldingRegisters,
        SendInputRegisters,
        Finished
    };
    Q_ENUM(MSendState)

    void ConnectToDevice(QString PortName,
                         QSerialPort::Parity Parity,
                         QSerialPort::BaudRate Baud,
                         QSerialPort::DataBits DataBits,
                         QSerialPort::StopBits StopBits,
                         int ResponseTime,
                         int NumberOfRetries);

    void ReadDevice();
    void WriteToDevice();
    void SaveOnDevice();
    void SetDeviceID(int ID);
    int GetDeviceID() const { return DeviceID; }
    void SendNewDeviceID(int ID, bool BoardCast);
    ~MDevice();

    MDevice::InputMode GetMode(int input);

    int GetDeviceVersion(){return InputRegisters.value(REG_DEVICE_VERSION);}
    int GetHardwareVersion(){return InputRegisters.value(REG_HARDWARE_VERSION);}

    void SetNumberRegister(int newNumberCoilsRegisters,
                           int newNumberDiscreteRegisters,
                           int newNumberHoldingRegisters,
                           int newNumberInputRegisters);

    void DisconnectDevice()
    {
        /*if(!ModbusDevice){
            ModbusDevice = new QModbusRtuSerialMaster(this);
        }*/
        ModbusDevice->disconnectDevice();
        delete ModbusDevice;
        ModbusDevice = nullptr;
    }

    void SetConnectionSettings(int Parity,
                               int Baud);

    void SetDeviceMode(int mode);
signals:
    void ReadFinished();
    void WriteFinished();
    void Error(QString errorString);
    void TransferStatus(MSendState state);
    void ConnectStateChanged(QModbusDevice::State state);

private slots:
    void readReady();
    void writeReady();
    void SendSaveCommand();
    void onStateChanged();

protected:
    enum MSendState SendState;

    int packetNumber = 0;

    QModbusDataUnit CoilsRegisters;
    QModbusDataUnit DiscreteRegisters;
    QModbusDataUnit HoldingRegisters;
    QModbusDataUnit InputRegisters;
    QModbusClient* ModbusDevice = nullptr;

    static const int StartCoilsRegisters = 0;
    static const int StartDiscreteRegisters = 10000;
    static const int StartHoldingRegisters = 40000;
    static const int StartInputRegisters = 30000;

    int NumberCoilsRegisters = 8;
    int NumberDiscreteRegisters = 0;
    int NumberHoldingRegisters = 10;
    int NumberInputRegisters = 2;

    int DeviceID = 10;

    bool save;
};

#endif // MDEVICE_H
