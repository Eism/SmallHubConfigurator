/****************************************************************************
**
** Copyright (C) 2015 The Qt Company Ltd.
** Contact: http://www.qt.io/licensing/
**
** This file is part of the examples of the QtSerialBus module.
**
** $QT_BEGIN_LICENSE:BSD$
** You may use this file under the terms of the BSD license as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include <QDebug>
#include <QMainWindow>
#include <QSettings>
#include <QString>

#define SETTINGS_FILE_NAME "settings.conf"
#define CONFIGURATION_CATEGORY QString("configuration")

SettingsDialog::SettingsDialog(bool settingsOnDevice, MDevice* _mDevice, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);
    /*
     * Считывание настроек
    */
    QSettings *settings = new QSettings(SETTINGS_FILE_NAME,QSettings::IniFormat);
    settings->beginGroup(CONFIGURATION_CATEGORY);

    m_settings.parity = settings->value("parity").toInt();
    if (m_settings.parity > 0)
        ui->parityCombo->setCurrentIndex(m_settings.parity-1);
    else ui->parityCombo->setCurrentIndex(m_settings.parity);

    m_settings.baud = settings->value("baud",115200).toInt();
    ui->baudCombo->setCurrentIndex(ui->baudCombo->findText(QString::number(m_settings.baud)));

    m_settings.deviceMode = settings->value("deviceMode",2).toInt();
    ui->deviceModeCombo_2->setCurrentIndex(m_settings.deviceMode - 2);
    settings->endGroup();

    mDevice = _mDevice;
    if (settingsOnDevice)
    {
        ui->applyButton->hide();
    }else
    {
        ui->pushButtonSave->hide();
        ui->deviceModeCombo_2->hide();
        ui->label_DeviceMode->hide();
    }

    if (mDevice) {
        connect(mDevice,SIGNAL(WriteFinished()),this,SLOT(SaveDataOnDevice()));
    }

    msgBox = new QMessageBox;
    connect(msgBox, &QMessageBox::finished,[this](){
        disconnect(mDevice,&MDevice::Error,this,&SettingsDialog::ErrorSaveDataOnDevice);
        disconnect(mDevice,SIGNAL(WriteFinished()),this,SLOT(SaveDataOnDevice()));
        emit FinishSaveOnDevice();
        msgBox->close();
        close();
    });
}

SettingsDialog::~SettingsDialog()
{
    if (mDevice){
        disconnect(mDevice,SIGNAL(Error(QString)),this,SLOT(ErrorSaveDataOnDevice()));
        disconnect(mDevice,SIGNAL(WriteFinished()),this,SLOT(SaveDataOnDevice()));
    }
    delete msgBox;
    delete ui;
}

SettingsDialog::Settings SettingsDialog::settings() const
{
    return m_settings;
}

void SettingsDialog::SaveDataOnDevice()
{
    msgBox->setText(tr("Настройки сохранены, устройство перезапущено. Пожалуйста переподключитесь с новыми настройками."));
    msgBox->addButton(tr("Да"), QMessageBox::YesRole);
    msgBox->show();
}

void SettingsDialog::ErrorSaveDataOnDevice()
{
    ui->labelLog->setText("Ошибка сохранения. Попробуйте еще раз!");
}

void SettingsDialog::on_applyButton_clicked()
{
    m_settings.parity = ui->parityCombo->currentIndex();
    if (m_settings.parity > 0)
        m_settings.parity++;
    m_settings.baud = ui->baudCombo->itemText(ui->baudCombo->currentIndex()).toInt();
    // см. документацию по магическим числам
    m_settings.dataBits = 8;
    m_settings.stopBits = 1;
    m_settings.responseTime = 200;
    m_settings.numberOfRetries = 3;
    m_settings.deviceMode = ui->deviceModeCombo_2->currentIndex() + 2;
    emit SaveNewConnectionSettings(m_settings);

    QSettings *settings = new QSettings(SETTINGS_FILE_NAME,QSettings::IniFormat);
    settings->beginGroup(CONFIGURATION_CATEGORY);
    settings->setValue("parity", m_settings.parity);
    settings->setValue("baud", m_settings.baud);
    settings->setValue("deviceMode", m_settings.deviceMode);
    settings->endGroup();
    settings->sync();

    close();
}

void SettingsDialog::on_pushButtonSave_clicked()
{
    if (!mDevice) return;
    m_settings.parity = ui->parityCombo->currentIndex();
    m_settings.baud = ui->baudCombo->currentIndex();
    m_settings.deviceMode = ui->deviceModeCombo_2->currentIndex() + 2;

    mDevice->SetConnectionSettings(m_settings.parity, m_settings.baud);
    mDevice->SetDeviceMode(m_settings.deviceMode);
    mDevice->SaveOnDevice();

    QSettings *settings = new QSettings(SETTINGS_FILE_NAME,QSettings::IniFormat);
    settings->beginGroup(CONFIGURATION_CATEGORY);
    settings->setValue("parity", m_settings.parity);
    settings->setValue("baud", m_settings.baud);
    settings->setValue("deviceMode", m_settings.deviceMode);
    settings->endGroup();
    settings->sync();
}
